// Lấy thông tin từ form
var danhSachSV = [];
var DSSV_LOCAL = "DSSV_LOCAL";
// khi user load trang => lấy dữ liệu localStorage
var jsonData = localStorage.getItem(DSSV_LOCAL);
if (jsonData != null) {
  danhSachSV = JSON.parse(jsonData);
  renderDSSV(danhSachSV);
}
console.log("jsonData: ", jsonData);

function themSV() {
  danhSachSV.push(layThonTinTuForm());

  // localStorage : lưu trữ, JSON : convert data
  // convert data
  var dataJson = JSON.stringify(danhSachSV);
  // lưu vào localStorage
  localStorage.setItem(DSSV_LOCAL, dataJson);

  // render danhSachSV ra giao diện
  renderDSSV(danhSachSV);
  resetForm();
}

function xoaSV(id) {
  //   danhSachSV.splice(i, 1);
  var index = -1;
  for (var i = 0; i < danhSachSV.length; i++) {
    if (danhSachSV[i].ma == id) {
      index = i;
    }
  }
  if (index != -1) {
    // console.log("index: ", danhSachSV.length);
    danhSachSV.splice(index, 1);
    // console.log("index: ", danhSachSV.length);
    renderDSSV(danhSachSV);
    var dataJson = JSON.stringify(danhSachSV);
    localStorage.setItem(DSSV_LOCAL, dataJson);
  }
}
function suaSV(id) {
  var viTri = danhSachSV.findIndex(function (item) {
    return item.ma == id;
  });
  var sv = danhSachSV[viTri];
  if (viTri != -1) {
    document.getElementById("txtMaSV").value = sv.ma;
    document.getElementById("txtTenSV").value = sv.ten;
    document.getElementById("txtEmail").value = sv.email;
    document.getElementById("txtPass").value = sv.matKhau;
    document.getElementById("txtDiemToan").value = sv.toan;
    document.getElementById("txtDiemLy").value = sv.ly;
    document.getElementById("txtDiemHoa").value = sv.hoa;
    document.getElementById("txtMaSV").disabled = true;
  }
}
function capNhat() {
  var sv = layThonTinTuForm();
  var viTri = danhSachSV.findIndex(function (item) {
    return item.ma == sv.ma;
  });
  danhSachSV[viTri] = sv;
  renderDSSV(danhSachSV);
}
function resetForm() {
  document.getElementById("formQLSV").reset();
}
